A bunch of small utilities for tasks I do frequently. Each is a stand-alone
executable file (in Linux systems). For more details about how to use each
of them, just call it with the `--help` command-line option.

- `cam`: Compresses a sequence of images and merges them into a PDF
- `pdfcrop`: Resizes a PDF's bounding box, thus "cropping" it
- `tob64`: Transforms a file into its base-64 string
- `updatepdfmetadata`: Updates a PDF's metadata
- `zipignore`: Compress a folder, ignoring all files specified in
  .gitignore. Useful for archiving Git repositories.

## To install

Method 1: clone repository, and copy files somewhere in your path.

### Method 2: use the install script

You can install this via the command-line with either curl or wget.

#### Using wget

> wget https://bitbucket.org/sylvainhalle/smallutilities/raw/master/install.sh - | bash

#### Using curl

> curl -L https://bitbucket.org/sylvainhalle/smallutilities/raw/master/install.sh | bash
