#! /bin/bash
set -e
tmpdir=/tmp/SmallUtilities
destination=/usr/local/bin
mkdir -p $tmpdir
wget -P $tmpdir https://bitbucket.org/sylvainhalle/smallutilities/get/HEAD.zip
unzip -j -o HEAD.zip -d $tmpdir
rm $tmpdir/HEAD.zip
chmod +x $tmpdir/*
cp `ls --ignore=install.sh --ignore README.md --ignore .gitignore $tmpdir` $destination
